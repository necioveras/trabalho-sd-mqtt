/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.mqtt.sensores;

import java.util.Random;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author necio
 */
public class SensorGenerico extends Thread {

    private String device;
    private String ipGateway;
    private int intervaloMin;
    private int intervaloMax;
    private JLabel visualValor; 
    
    public SensorGenerico(String device, String ipGateway, int intervaloMin, int intervaloMax, JLabel visual){
        this.device = device;
        this.ipGateway = ipGateway;
        this.intervaloMax = intervaloMax;
        this.intervaloMin = intervaloMin;
        this.visualValor = visual;
    }    
    
    public SensorGenerico(String device, String ipGateway){
        this.device = device;
        this.ipGateway = ipGateway;        
    }
    
    @Override
    public void run() {
        try{
            publicar();
        } catch (MqttException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.toString());            
        } catch (InterruptedException i){
            i.printStackTrace();
        }
    }    
   

    @Override
    public synchronized void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    public void publicar() throws MqttException, InterruptedException{
        
        while (true){
            
            int valor = new Random().nextInt() % (intervaloMax - intervaloMin);
            
            if (valor > intervaloMax)
                valor = intervaloMax;
            else if (valor < intervaloMin)
                valor = intervaloMin;

            String messageString = Integer.toString(valor);	   

            System.out.println("== START PUBLISHER ==");

            MqttClient client = new MqttClient("tcp://"+ ipGateway +":1883", MqttClient.generateClientId());
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload(messageString.getBytes());
            client.publish(device, message);

            System.out.println("\tMessage '"+ messageString +"' to " + device);
            client.disconnect();
            
            visualValor.setText(Integer.toString(valor));

            System.out.println("== END PUBLISHER ==");
            
            sleep(5000);
            
        }
              
    }
    
    public void publicar(boolean status) throws MqttException{
            
            String str;
            
            if (status)
                str = "on";
            else
                str = "off";   

            System.out.println("== START PUBLISHER ==");

            MqttClient client = new MqttClient("tcp://"+ ipGateway +":1883", MqttClient.generateClientId());
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload(str.getBytes());
            client.publish(device, message);

            System.out.println("\tMessage '"+ str +"' to " + device);
            client.disconnect();
                        
            System.out.println("== END PUBLISHER ==");            
        
              
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getIpGateway() {
        return ipGateway;
    }

    public void setIpGateway(String ipGateway) {
        this.ipGateway = ipGateway;
    }

    public int getIntervaloMin() {
        return intervaloMin;
    }

    public void setIntervaloMin(int intervaloMin) {
        this.intervaloMin = intervaloMin;
    }

    public int getIntervaloMax() {
        return intervaloMax;
    }

    public void setIntervaloMax(int intervaloMax) {
        this.intervaloMax = intervaloMax;
    }

    public JLabel getVisualValor() {
        return visualValor;
    }

    public void setVisualValor(JLabel visualValor) {
        this.visualValor = visualValor;
    }
    
    
    
}
