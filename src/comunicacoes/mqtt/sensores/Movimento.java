/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.mqtt.sensores;

import java.util.Random;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author necio
 */
public class Movimento extends SensorGenerico {

    private JLabel visualY; 
    
    public Movimento(String device, String ipGateway, int intervaloMin, int intervaloMax, JLabel visualX, JLabel visualY){
        super(device, ipGateway, intervaloMin, intervaloMax, visualX);
        this.visualY = visualY;
    }    
    
    @Override
    public void run() {
        try{
            publicar();
        } catch (MqttException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, e.toString());            
        } catch (InterruptedException i){
            i.printStackTrace();
        }
    }    
    
    
    @Override
    public void publicar() throws MqttException, InterruptedException{
        
        while (true){
            
            int valorX = new Random().nextInt() % (getIntervaloMax() - getIntervaloMin());
            int valorY = new Random().nextInt() % (getIntervaloMax() - getIntervaloMin());
            
            if (valorX > getIntervaloMax())
                valorX = getIntervaloMax();
            else if (valorX < getIntervaloMin())
                valorX = getIntervaloMin();

            if (valorY > getIntervaloMax())
                valorY = getIntervaloMax();
            else if (valorY < getIntervaloMin())
                valorY = getIntervaloMin();
                        
            String messageStringX = Integer.toString(valorX);	   
            String messageStringY = Integer.toString(valorY);	   

            System.out.println("== START PUBLISHER ==");

            MqttClient client = new MqttClient("tcp://"+ getIpGateway() +":1883", MqttClient.generateClientId());
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload((messageStringX + "/" + messageStringY).getBytes());
            client.publish(getDevice(), message);

            System.out.println("\tMessage '"+ messageStringX + "/" + messageStringY +"' to " + getDevice());
            client.disconnect();
            
            getVisualValor().setText(Integer.toString(valorX));
            visualY.setText(Integer.toString(valorY));

            System.out.println("== END PUBLISHER ==");
            
            sleep(5000);
            
        }
              
    }
    
}
