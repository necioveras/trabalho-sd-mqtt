/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.mqtt.atuadores;

import javax.swing.JLabel;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author necio
 */
public class Callback implements MqttCallback {
    
    JLabel jlabel; 
    
    public Callback(JLabel jlabel){
        this.jlabel = jlabel;
        
    }

    public void connectionLost(Throwable throwable) {
      System.out.println("Conexão perdida com o broken MQTT!!");
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
       //System.out.println("Mensagem recebida:\t"+ new String(mqttMessage.getPayload()) );
       String str = new String(mqttMessage.getPayload());
       if (str.equals("on"))
            jlabel.setText("Dispositivo ligado");
       else if (str.equals("off"))
           jlabel.setText("Dispositivo desligado");
       else{ //mensagem nao eh um texto de on/off
           jlabel.setText(str);
       }
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
    }

}
