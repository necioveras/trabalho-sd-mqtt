/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.mqtt.atuadores;

import javax.swing.JLabel;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author necio
 */
public class AtuadorGenerico {

    private String device;
    private String ipGateway;
    private JLabel visualValor; 
    
    public AtuadorGenerico(String device, String ipGateway, JLabel visual){
        this.device = device;
        this.ipGateway = ipGateway;
        this.visualValor = visual;
    }       
    
    public void subscrever(){
        
        System.out.println("== START SUBSCRIBER ==");
        try{
            MqttClient client=new MqttClient("tcp://"+ ipGateway +":1883", MqttClient.generateClientId());
            client.setCallback( new Callback(visualValor) );		    
            client.connect();
            client.subscribe(device);
        } catch (MqttException m){
            m.printStackTrace();
        }
              
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getIpGateway() {
        return ipGateway;
    }

    public void setIpGateway(String ipGateway) {
        this.ipGateway = ipGateway;
    }    

    public JLabel getVisualValor() {
        return visualValor;
    }

    public void setVisualValor(JLabel visualValor) {
        this.visualValor = visualValor;
    }
    
    
    
}
