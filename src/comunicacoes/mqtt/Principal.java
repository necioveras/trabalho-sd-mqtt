/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comunicacoes.mqtt;

import comunicacoes.mqtt.visoes.Atuadores;
import comunicacoes.mqtt.visoes.ControleUniversal;
import comunicacoes.mqtt.visoes.Sensores;
import javax.swing.JOptionPane;

/**
 *
 * @author necio
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
         String str = (String) JOptionPane.showInputDialog(null, "Deseja iniciar um sensor ou um atuador?", "Sensor/Atuador", JOptionPane.QUESTION_MESSAGE,
                null, new String[]{"Sensores", "Atuadores", "Controle universal"} , null);
        
        if (str.equals("Sensores")){
            Sensores s = new Sensores();
            s.setLocationRelativeTo(null);
            s.setVisible(true);
            s.prepararConteudo(-1);
        }else if (str.equals("Atuadores")){            
            Atuadores a = new Atuadores();
            a.setVisible(true);
            a.setLocationRelativeTo(null);
            a.prepararConteudo(-1);
        }
        else{
            ControleUniversal c = new ControleUniversal();
            c.setVisible(true);
            c.setLocationRelativeTo(null);
        }
        
        
    }
    
}
